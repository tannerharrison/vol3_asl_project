"""
Created by Tanner Harrison on 10/15/20

The idea behind this script is that we have two different datasets. We would like to extract a small
sample from each dataset to form a smaller dataset that is easier to work with.

This script assumes that each of the datasets are in the dataset folder


The first dataset is from this Kaggle competition: https://www.kaggle.com/grassknoted/asl-alphabet
The second dataset is from this professor's website: http://empslocal.ex.ac.uk/people/staff/np331/index.php?section=FingerSpellingDataset

"""


import os
from shutil import copyfile

PATH_TO_NEW_DATASET = "../dataset/small_dataset"

FIRST_DATASET = "../dataset/archive/asl_alphabet_train/asl_alphabet_train"
SECOND_DATASET = "../dataset/dataset5"

#all letters in the alphabet except J and Z. We don't care about these letters since their signs require movement
alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y"]

def create_directory(path):
    """Creates a directory and ignores the FileExistsError if the directory already exists"""
    try:
        os.mkdir(path)
    except FileExistsError:
        # in the case that the folder exists, we don't care
        pass

def create_new_file_structure():
    """Creates the new file structure for the new dataset"""
    create_directory(PATH_TO_NEW_DATASET)
    for letter in alphabet:
        create_directory(PATH_TO_NEW_DATASET + "/" + letter)


def pull_samples_from_first_dataset(files_to_copy = 25):
    """Copies images from the Kaggle dataset to the new dataset"""
    for letter in alphabet:
        letter_directory = FIRST_DATASET + "/" + letter
        files = os.listdir(letter_directory)
        for i in range(files_to_copy):
            src = letter_directory + "/" + files[i]
            dest = PATH_TO_NEW_DATASET + "/" + letter + "/" + files[i]
            copyfile(src, dest)

def pull_samples_from_second_dataset(files_to_copy = 5):
    """Copies images from the other dataset into the new dataset"""
    subjects = ["A", "B", "C", "D", "E"]

    for subject in subjects:
        for letter in alphabet:
            letter_directory = SECOND_DATASET + "/" + subject + "/" + letter.lower()
            files = os.listdir(letter_directory)

            #we make sure that we are only getting color pictures, as opposed to depth pictures
            files = [x for x in files if x.startswith("color")]

            for i in range(files_to_copy):
                src = letter_directory + "/" + files[i]
                dest = PATH_TO_NEW_DATASET + "/" + letter + "/" + subject + "_" + files[i]
                copyfile(src, dest)

def main():
    create_new_file_structure()
    pull_samples_from_first_dataset()
    pull_samples_from_second_dataset()


if __name__ == "__main__":
    main()


