# How to set up the datasets for this repository

Since Bitbucket won't allow too much data in a git repository, simply cut and paste the datasets into this folder.

Here are the links to the datasets:
* https://www.kaggle.com/grassknoted/asl-alphabet
* http://empslocal.ex.ac.uk/people/staff/np331/index.php?section=FingerSpellingDataset


    